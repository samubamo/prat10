/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica {
    public static void main(String[] args) {
        String os = System.getProperty("os.name");
        Runtime rt = Runtime.getRuntime();//instaciamos a class Runtime;
        int mg = 1048576;
        
        String fmt = "%.1f";
        System.out.println("Sistema Operacional: " + os);
        System.out.println("Numero de Processadors: " + rt.availableProcessors());
        
        System.out.println(String.format("Memoria Total: " + fmt, (double)rt.totalMemory()/mg) + " MB");
        System.out.println(String.format("Memoria Livre: " + fmt, (double)rt.freeMemory()/mg) + " MB");
        System.out.println(String.format("Memoria Total: " + fmt, (double)rt.maxMemory()/mg) + " MB");
    }
}
